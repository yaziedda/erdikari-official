# HACKATON ENERGI NEGERI (ERDIKARI TEAM) #

Repo ini dibuat untuk **HACKATON ENERGI NEGERI**  yang di selenggarakan oleh **DEWAN ENERGI MAHASISWA (DEM)** <a href="https://hackathon.demindonesia.org/" target="_blank">Official Link</a>

ERDIKARI adalah sistem perniagaan berbasis marketplace dapat dijadikan sebagai alternatif bagi Indonesia untuk memanfaatkan pengembangan besarnya potensi sumber energi alternatif, dijadikan sebagai media promosi, komunikasi, edukasi dan informasi. Manfaat yang dirasakan oleh masyarakat secara langsung dan tidak langsung memberi pengaruh positif, terutama dari semakin luasnya akses pengembangan energi alternatif tenaga listrik di Indonesia serta memacu pengadaan produksi energi alternatif secara masif di Indonesia sehingga masyarakat bisa mendapatkan listrik yang murah dan ramah lingkungan.  

## Demo ##
<a href="https://drive.google.com/file/d/1hZG6sC7Oz9MB84TDqiwhLV5Yf3QjWhdG/view?usp=sharing" target="_blank">Erdikari Android App</a> (.apk)<br/>
<a href="https://erdikari-admin.clikini.com" target="_blank">Erdikari Web Admin</a> (website)<br/>
<a href="https://www.getpostman.com/collections/b8efba8e5435a27491ed" target="_blank">API Collection</a><br/>
<a href="https://www.figma.com/file/zWK325QKIlsONqiKR4ZuHe/ERDIKARI?node-id=43%3A54" target="_blank">Mockup and Prototype</a><br/>
<a href="https://drive.google.com/file/d/1rVHqMyP7TdyEDFbFqdmTTw4Yg22Id6Js/view?usp=sharing" target="_blank">Persentation</a><br/>

## Webadmin Credential ##

Url : <a href="https://erdikari-admin.clikini.com" target="_blank">https://erdikari-admin.clikini.com</a><br />
Superadmin:
- username : `admin`
- password : `123`

## Repository ##
**Android** <a href="https://gitlab.com/yaziedda/erdikari-android" target="_blank">https://gitlab.com/yaziedda/erdikari-android</a><br />
**Web Admin Repo** <a href="https://gitlab.com/yaziedda/erdikari-admin" target="_blank">https://gitlab.com/yaziedda/erdikari-admin</a><br />
**REST API** <a href="https://gitlab.com/yaziedda/erdikari-api" target="_blank">https://gitlab.com/yaziedda/erdikari-api</a><br />

## Thanks for ##
**Midtrans (Payment Gateway)**  <a href="https://midtrans.com/" target="_blank">https://midtrans.com</a><br/>
**Rajaongkir (Ekspedisi)**  <a href="https://rajaongkir.com/" target="_blank">https://rajaongkir.com</a><br/>
